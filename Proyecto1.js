var numSquares = 6;
var colors = [];
var pickedColor;
var squares = document.querySelectorAll('.square');
var colorDisplay = document.getElementById('colorDisplay');
var messageDisplay = document.querySelector('#message');
var h1 = document.querySelector('h1');
var resetButton = document.querySelector('#reset');
var modeButtons = document.querySelectorAll('.mode');
var clickedColor;
var game = {};
var movimientos = 0;
var aciertos = 0;
var fallas = 0;

//APUNTANDO HTML
var mostrarAciertos = document.getElementById(`aciertos`);
var mostrarFallas = document.getElementById(`fallas`);

game.init = function () {
  setupModeButtons();
  setupSquares();
  reset();
};

game.init();

function setupModeButtons() {
  for (var i = 0; i < modeButtons.length; i++) {
    modeButtons[i].addEventListener('click', function () {
      modeButtons[0].classList.remove('selected');
      modeButtons[1].classList.remove('selected');
      this.classList.add('selected');
      this.textContent === 'Easy' ? (numSquares = 3) : (numSquares = 6);
      reset();
    });
  }
}

function setupSquares() {
  for (var i = 0; i < squares.length; i++) {
    squares[i].addEventListener('click', function () {
      if (this.style.backgroundColor === pickedColor) {
        messageDisplay.textContent = 'Correcto';
        resetButton.textContent = '¿Repetir?';
        changeColors(pickedColor);
        h1.style.background = pickedColor;
        aciertos++;
        mostrarAciertos.innerHTML = `Aciertos: ${aciertos}`;

        if (aciertos === 3) {
          mostrarAciertos.innerHTML = `Aciertos: ${aciertos} ganaste`;
          i = squares.length;
        }
      } else {
        this.style.backgroundColor = '#232323';
        messageDisplay.textContent = 'Try Again';
        fallas++;
        mostrarFallas.innerHTML = `Fallas: ${fallas}`;

        if (fallas === 3) {
          mostrarFallas.innerHTML = `fallas: ${fallas} perdiste`;
          i = squares.length;
        }
      }
    });
  }
}

//FUNCION DEL RESETEAR JUEGO
function reset() {
  colors = generateRandomColors(numSquares);

  //Elegir un nuevo color aleatorio de la matriz
  pickedColor = pickColor();

  //cambiar la pantalla de color para que coincida con el color elegido
  colorDisplay.textContent = pickedColor;
  resetButton.textContent = 'New Colors';
  messageDisplay.textContent = '';

  //cambiar los colores de los cuadrados
  for (var i = 0; i < squares.length; i++) {
    if (colors[i]) {
      squares[i].style.display = 'block';
      squares[i].style.backgroundColor = colors[i];
    } else {
      squares[i].style.display = 'none';
    }
  }
  h1.style.backgroundColor = 'BLACK';
}

//interacción del boton para resetear
resetButton.addEventListener('click', function () {
  reset();
});

function changeColors(color) {
  //Recorrer todos los cuadrados
  for (var i = 0; i < squares.length; i++) {
    //cambiar cada color para que coincida con el color dado
    squares[i].style.backgroundColor = color;
  }
}
//relacionar el color seleccionado
function pickColor() {
  var random = Math.floor(Math.random() * colors.length);
  return colors[random];
}

//
function generateRandomColors(num) {
  var arr = [];
  for (var i = 0; i < num; i++) arr.push(randomColor());
  return arr;
}
n;
// ASIGNACIÓN RANDOM DE COLORES
// Math.floor: redondea numero entero anterior
function randomColor() {
  var r = Math.floor(Math.random() * 256);
  var g = Math.floor(Math.random() * 256);
  var b = Math.floor(Math.random() * 256);
  return 'rgb(' + r + ', ' + g + ', ' + b + ')';
}
